﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour
{
#if UNITY_ANDROID
    public FixedJoystick joystick;
#endif
    [HideInInspector]
    public CharacterController characterController;
    public Animator animator;

    public float walkSpeed = 1.5f;
    public float runSpeed = 3.0f;
    public float rotationSpeed = 50.0f;
    public float gravity = -30.0f;
    public float jumpHeight = 3f;

    [Tooltip("Only useful with Follow and Independent Rotation - third - person camera control")]
    public bool followCameraForward = false;
    public float turnRate = 200.0f;
    private Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }
    void Update()
    {
        //Move();
    }
    public void Move()
    {
#if UNITY_STANDALONE
    float h = Input.GetAxis("Horizontal");
    float v = Input.GetAxis("Vertical");
#endif

#if UNITY_ANDROID
        float h = joystick.Horizontal;
        float v = joystick.Vertical;
#endif

#if UNITY_STANDALONE
        float speed = walkSpeed;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = runSpeed;
        }
#endif

#if UNITY_ANDROID
        float speed = runSpeed;
#endif


        if (followCameraForward)
        {
            if (v > 0.1 || v < -0.1 || h > 0.1 || h < -0.1)
            {
                Vector3 eu = Camera.main.transform.rotation.eulerAngles;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, eu.y, 0.0f), turnRate * Time.deltaTime);
            }
        }
        
        else
        {
            transform.Rotate(0.0f, h * rotationSpeed * Time.deltaTime, 0.0f);
        }

        characterController.Move(transform.forward * v * speed * Time.deltaTime);

        Vector3 forward = transform.TransformDirection(Vector3.forward).normalized;
        forward.y = 0.0f;
        Vector3 right = transform.TransformDirection(Vector3.right).normalized;
        right.y = 0.0f;

        if (animator != null)
        {
            if (followCameraForward)
            {
                characterController.Move(forward * v * speed * Time.deltaTime + right * h * speed * Time.deltaTime);
                animator.SetFloat("PosX", h * speed / runSpeed);
                animator.SetFloat("PosZ", v * speed / runSpeed);
            }
            else
            {
                characterController.Move(forward * v * speed * Time.deltaTime);
                animator.SetFloat("PosX", 0);

                animator.SetFloat("PosZ", v * speed / runSpeed);
            }
            
        }

        velocity.y += gravity * Time.deltaTime;
        characterController.Move(velocity * Time.deltaTime);

        if (characterController.isGrounded && velocity.y < 0)
        velocity.y = 0f;

        if (Input.GetKey(KeyCode.Space))
        {
            Jump();
        }
    }
    void Jump()
    {
        if (characterController.isGrounded)
        {
            animator.SetTrigger("Jump");
            velocity.y += Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

}
