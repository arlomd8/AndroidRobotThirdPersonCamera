﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectible : MonoBehaviour
{
    public GameObject uWin;
#if UNITY_ANDROID
    public GameObject joystick;
    public GameObject TouchField;
#endif

    private void Start()
    {
        uWin.SetActive(false);
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            print("Collide");
            Destroy(gameObject);
            uWin.SetActive(true);
            //joystick.SetActive(false);
            //TouchField.SetActive(false);
        }
    }
    
}
