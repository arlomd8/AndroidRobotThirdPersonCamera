﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public void NextButton()
    {
        SceneManager.LoadScene(1);
    }

    //public void NextButton2()
    //{
    //    SceneManager.LoadScene(2);
    //}
    public void OKButton()
    {
        Application.Quit();
    }
}
