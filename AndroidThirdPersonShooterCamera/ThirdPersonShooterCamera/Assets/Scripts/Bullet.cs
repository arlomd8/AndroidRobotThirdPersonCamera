﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public bool isCollide = false;
    public Rigidbody rb;
    public float speed;
    void Start()
    {
        StartCoroutine("BulletClear");

        rb.AddForce(Vector3.forward * speed);
    }
    public void OnCollisionEnter(Collision collision)
    {
        isCollide = true;
        if (collision.gameObject.CompareTag("PENGHALANGA"))
        {
            Destroy(gameObject);
        }
        //GetComponent<Rigidbody>().AddForce(-collision.contacts[0].normal +
         //new Vector3(Random.Range(-1, 1), Random.Range(-1, 1)));
    }
    private IEnumerator BulletClear()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
